package com.haseloff.konsolenausgabe;

public class KonsolenAusgabe2 {

	public static void main(String[] args) {
		// Aufgabe 1 //
		String zweiSterne = "**";
		String einStern = "*";
		String einSternZwei = "*";
		
		System.out.printf( "%5s\n", zweiSterne );
		System.out.printf( "%-3s", einStern );
		System.out.printf( "%5s\n", einStern );
		System.out.printf( "%-5s%3s\n", einStern , einSternZwei );
		System.out.printf( "%5s\n", zweiSterne );
		
		// Aufgabe 2
		String gleich = "=";
		String a = "\n0!";
		String b = "\n1";
		
		// linksbŁndig mit 20 Stellen
		System.out.printf( "%-5s%-5s", a , gleich); // 0!   = 


	}

}
